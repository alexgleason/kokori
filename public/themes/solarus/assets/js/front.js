function validateCaptcha(token) {
  document.getElementById("form-contact").submit();
}

$(document).ready(function () {
  // Scrolling
  var $headerNode = $("header");
  var $coverNode = $(".shortcode.shortcode-cover");
  var heightHeader = $headerNode.height();
  if ($coverNode.length > 0) {
    $("body").addClass("has-header-cover");
    $(window).on('scroll', function() {
      var scroll = $(window).scrollTop();
      if (scroll < heightHeader) {
        $headerNode.addClass("header-transparent");
        $coverNode.addClass("cover-top");
      } else {
        $headerNode.removeClass("header-transparent");
        $coverNode.removeClass("cover-top");
      }
    });
    $(window).trigger('scroll');
  }
  // Effect In
  $('.effect-in').each(function () {
    var element = this;
    var height = $(window).outerHeight(true);
    var effect = $(this).data('effect-in');
    if (typeof(effect) != 'undefined') {
      new Waypoint({
        element: this,
        offset: height / 2,
        handler: function (direction) {
          $(element).addClass("effect-in-start animated " + effect);
        }
      });
    }
  });
  // Summary
  $("[data-toggle=heading-scroll]").on("click", function () {
    var linkNode = $(this);
    var containerNode = $('.page');
    var level = linkNode.data("level");
    var index = linkNode.data("index");
    var targetsNode = $(containerNode).find('h' + level);
    var targetNode = targetsNode.get(index);
    if ($(targetNode).length == 1) {
      var scrollTop = $(targetNode).offset().top - $('header').outerHeight(true) - 50;
      $("html, body").animate({
        "scrollTop": scrollTop
      }, 1000);
    }

    return false;
  });
  // Downloads
  $("[data-toggle=download]").on('click', function() {
    var id = $(this).data('id');
    var title = $(this).data('modal-title');
    var buttonLabel = $(this).data('modal-button-label');
    var category = $(this).data('category');
    var file = $(this).data('file');
    var language = $(this).data('language');
    $.ajax({
      url : '/ajax/modal',
      type: 'POST',
      dataType:'json',
      data: {
        'action': 'download',
        'id': id,
        'title': title,
        'buttonLabel': buttonLabel,
        'category': category,
        'file': file,
        'language': language
      },
      success : function(response) {
        $("#modal-download").find('.modal-content').html(response['content']);
        $("#modal-download").modal('show');
      }
    });
    return false;
  });
  // Menu
  $("[data-toggle=menu]").each(function () {
    var effectIn = $(this).data("effect-in");
    $(this).find(".navbar-nav li").on("mouseenter", function() {
      var submenuNode = $(this).find(".navbar-submenu").first();
      if ($(submenuNode).length > 0) {
        $(submenuNode).addClass("animated " + effectIn);
      }
    });
  });
  // Isotope
  function initIsotope() {
    var sortingElement = $("[data-toggle=sorting]").find("input:checked");
    var ascending = $(sortingElement).data("ascending");
    var sortAscending = true;
    if (ascending !== "asc") {
      sortAscending = false;
    }
    $("[data-toggle=isotope]").isotope({
      filter: function() {
        var element = $(this);
        var statusElement = true;
        var continueFilters = true;
        // Browse filters
        $("[data-toggle=filter]").each(function () {
          if (continueFilters) {
            var type =  $(this).data("type");
            // Browse checkboxes
            var subStatusElement = true;
            $(this).find("input:checked").each(function () {
              var className = type + "-" + $(this).val();
              if ($(element).hasClass(className) === false) {
                subStatusElement = false;
                statusElement = false;
                //continueFilters = false;
              } else {
                continueFilters = true;
              }
            });
          }
        });
        // Search in title
        if ( $("[data-toggle=search]").length > 0) {
          var search = $("[data-toggle=search]").val().toLowerCase();
          if (statusElement && search != '') {
            var title = $(element).find('h2').text().toLowerCase();
            var pos = title.indexOf(search)
            if (pos === -1) {
              statusElement = false;
            }
          }
        }
        // Search in excerpt
        if (statusElement === false && search != '') {
          var excerpt = $(element).find('.excerpt').text().toLowerCase();
          var pos = excerpt.indexOf(search);
          if (pos !== -1) {
            statusElement = true;
          }
        }
        if (statusElement) {
          return true;
        }
        return false;
        /*
        var statusElement = "initial";
        $("[data-toggle=filter]").each(function () {
          if (statusElement !== "hide") {
            var type =  $(this).data("type");
            $(this).find("input:checked").each(function () {
              var className = type + "-" + $(this).val();
              if ($(element).hasClass(className) !== false) {
                statusElement = "show";
              } else {
                statusElement = "hide";
              }
            });
          }
        });
        if (statusElement === "show") {
          return true;
        }
        return false;
        */
      },
      sortBy: $(sortingElement).val(),
      sortAscending: sortAscending,
      getSortData: {
        date: ".date",
        alpha: ".alpha"
      }
    });
  }
  /*
  function getFiltersClasses() {
    var classes = "";
    var count = 0;
    $("[data-toggle=filter]").each(function () {
      if ($(this).is(":checked")) {
        count++;
        if (classes !== "") {
          classes = classes + ", ";
        }
        classes = classes + "." + $(this).data("type") + "-" + $(this).val();
      }
    });
    if (count === 0) {
      classes = ".none";
    }

    return classes;
  }
  */
  initIsotope();
  $("[data-toggle=sorting] input").each(function () {
    $(this).on("change", function() {
      initIsotope();
    });
  });
  $("[data-toggle=filter] input").each(function () {
    $(this).on("change", function() {
      initIsotope();
    });
  });
  $("[data-toggle=search]").each(function () {
    $(this).on("keyup", function() {
      initIsotope();
    });
  });
  // Paroller
  $("[data-toggle=parallax]").each(function () {
    $(this).paroller();
  });
  // Download links
  $("[data-toggle=download]").on("click", function() {
    var filename = $(this).data("filename");
    $(this).attr("download", filename);
  });
  // Fancybox
  $().fancybox({
    selector : ".owl-item:not(.cloned) .fancybox"
  });
  // Owl carousel
  $(".owl-carousel").each(function () {
    var element = this;
    var items = $(element).data("nb-items-visible");
    $(element).owlCarousel({
      loop: true,
      margin: 10,
      nav: true,
      dots: true,
      loop: false,
      items: items,
      slideBy: items
    });
  });
});

$(function () {
  jQuery("img").each(function () {
    var $img = jQuery(this);
    var imgID = $img.attr("id");
    var imgClass = $img.attr("class");
    var imgURL = $img.attr("src");

    jQuery.get(imgURL, function (data) {
      // Get the SVG tag, ignore the rest
      var $svg = jQuery(data).find("svg");

      // Add replaced image"s ID to the new SVG
      if (typeof imgID !== "undefined") {
        $svg = $svg.attr("id", imgID);
      }
      // Add replaced image"s classes to the new SVG
      if (typeof imgClass !== "undefined") {
        $svg = $svg.attr("class", imgClass + " replaced-svg");
      }

      // Remove any invalid XML tags as per http://validator.w3.org
      $svg = $svg.removeAttr("xmlns:a");

      // Check if the viewport is set, else we gonna set it if we can.
      if (!$svg.attr("viewBox") && $svg.attr("height") && $svg.attr("width")) {
        $svg.attr("viewBox", "0 0 " + $svg.attr("height") + " " + $svg.attr("width"))
      }

      // Replace image with new SVG
      $img.replaceWith($svg);

    }, "xml");

  });
});
