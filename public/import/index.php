<?php
// Variables
$host = 'localhost';
$user = 'root';
$password = 'root';
$database = 'zeldasol';
$prefix = 'wp_';
$directory = 'old';


// SCRIPT
$dir = __DIR__  . '/' . $directory;
// Remove directory
if (is_dir($dir)) {
  $dir_iterator = new RecursiveDirectoryIterator($dir);
  $iterator = new RecursiveIteratorIterator($dir_iterator, RecursiveIteratorIterator::CHILD_FIRST);
// On supprime chaque dossier et chaque fichier	du dossier cible
  foreach($iterator as $file){
    $file->isDir() ? rmdir($file) : unlink($file);
  }
// On supprime le dossier cible
  rmdir($dir);
}
// Create main directory
if (is_dir($dir) == false) {
  $result = mkdir($dir);
}
$link = mysqli_connect($host, $user, $password, $database);
if (!$link) {
  echo "Erreur : Impossible de se connecter à MySQL." . PHP_EOL;
  exit;
}
$sql = "SELECT * FROM " . $prefix . "posts AS p";
$sql = $sql . " INNER JOIN " . $prefix . "users AS u ON p.post_author = u.ID";
$sql = $sql . " WHERE post_type = 'post' AND post_status='publish'";
$results = $link->query($sql, MYSQLI_USE_RESULT);
while ($row = $results->fetch_assoc()) {
  $dateItems = explode(" ", $row['post_date']);
  $date = $dateItems[0];
  $dateItems = explode("-", $dateItems[0]);
  $slug = utf8_encode($row['post_title']);
  $slug = str_replace("é", "e", $slug);
  $slug = str_replace("è", "e", $slug);
  $slug = str_replace("à", "a", $slug);
  $slug = str_replace("â", "a", $slug);
  $slug = str_replace("ê", "e", $slug);
  $slug = str_replace("ë", "e", $slug);
  $slug = str_replace("û", "u", $slug);
  $slug = str_replace("ü", "u", $slug);
  $slug = str_replace("ô", "o", $slug);
  $slug = str_replace("ç", "c", $slug);
  $slug = str_replace("ç", "c", $slug);
  $slug = str_replace("'", "-", $slug);
  $slug = str_replace("î", "i", $slug);
  $slug = $dateItems[0] . '-'.$dateItems[1].'-'.$dateItems[2].'-'.slugify($slug);
  // Year directory
  if (is_dir($dir . '/' . $dateItems[0]) == false) {
    $result = mkdir($dir . '/' . $dateItems[0]);
  }
  // Month directory
  if (is_dir($dir . '/' . $dateItems[0]. '/' . $dateItems[1]) == false) {
    $result = mkdir($dir . '/' . $dateItems[0] . '/' . $dateItems[1]);
  }
  $filename = $slug;

  if ($filename !== '2018-03-28-link-et-marine') {
    //continue;
  }
  $row['post_content'] = utf8_encode($row['post_content']);
  $excerpt = strip_tags($row['post_content']);
  $excerpt = strWordCut($excerpt, 150);
  $author = $row['display_name'];
  $title = $row['post_title'];
  $title = utf8_encode($title);
  $data = array(
    "authors" => array($author),
    "categories" => array('solarus'),
    "creation_date" => $date,
    "excerpt"=> $excerpt,
    "id"=> $slug,
    "image_thumbnail" => "../../../../../assets/blog/default_article_thumbnail.jpg",
    "page_override" => array(
      "meta_description" => "Description MOS poutre",
      "meta_title" => $title,
      "slug" => $slug,
      "title" => $title
    ),
    "page_slug" => $slug,
    "publish" => true,
    "title" =>  $title
  );
  $data1 = $data;
  $data = json_encode($data, JSON_PRETTY_PRINT);
  $data = str_replace("\u00e8", "è", $data);
  $data = str_replace("\u00e9", "é", $data);
  $data = str_replace("\u00e2", "â", $data);
  $data = str_replace("\u00e0", "à", $data);
  $data = str_replace("\u00ea", "ê", $data);
  $data = str_replace("\u00eb", "ë", $data);
  $data = str_replace("\u00f9", "ù", $data);
  $data = str_replace("\u00ee", "ù", $data);
  $data = str_replace("\u00f4", "ô", $data);
  $data = str_replace("\u00e7", "ç", $data);
  $data = str_replace("\u00c7", "Ç", $data);
  $data = str_replace("\u0092", "'", $data);
  $data = str_replace("\u00fb", "û", $data);
  $path = $dir . '/' . $dateItems[0] . '/' . $dateItems[1] . '/' . $filename . '.json';
  file_put_contents($path, $data);
  $content = $row['post_content'];
  $regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";
  if (preg_match_all("/$regexp/siU", $content, $matches)) {
    foreach($matches[2] as $picture) {
      $extension = pathinfo($picture, PATHINFO_EXTENSION);
      if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'gif') {
        $path = $dir . '/' . $dateItems[0] . '/' . $dateItems[1] . '/images';
        if (is_dir($path) == false) {
          $result = mkdir($path);
        }
        $filenameImage = basename($picture);
        $contentPicture = file_get_contents($picture);
        file_put_contents($path . '/' . $filenameImage, $contentPicture);
        $newPath = '/images/' . $filenameImage;
        $newUrl = '../data/fr/entities/article/old/' . $dateItems[0] . '/' . $dateItems[1] . '/images/' . $filenameImage;
        $content = str_replace($picture, $newUrl, $content);
      }
    }
  }
  $regexp = "<img\s[^>]*src=(\"??)([^\" >]*?)\\1[^>]*>";
  if (preg_match_all("/$regexp/siU", $content, $matches)) {
    foreach($matches[2] as $picture) {
      $path = $dir . '/' . $dateItems[0] . '/' . $dateItems[1] . '/images';
      if (is_dir($path) == false) {
        $result = mkdir($path);
      }
      $filenameImage = basename($picture);
      $contentPicture = file_get_contents($picture);
      file_put_contents($path . '/' . $filenameImage,  $contentPicture);
      $newPath = '/images/' . $filenameImage;
      $content = str_replace($picture, $newPath, $content);
    }
  }
  $path = $dir . '/' . $dateItems[0] . '/' . $dateItems[1] . '/' . $filename . '.md';
  file_put_contents($path, $content);
  // Create directories
  if (is_dir($dir) == false) {
    $result = mkdir($dir);
  }
}

mysqli_close($link);


function slugify($text)
{
  $text_old = $text;
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    var_dump($text_old);exit();
    return 'n-a';
  }

  return $text;
}

function strWordCut($string,$length,$end='....')
{
  $string = strip_tags($string);

  if (strlen($string) > $length) {

    // truncate string
    $stringCut = substr($string, 0, $length);

    // make sure it ends in a word so assassinate doesn't become ass...
    $string = substr($stringCut, 0, strrpos($stringCut, ' ')).$end;
  }
  return $string;
}
?>                     