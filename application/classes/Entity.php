<?php
class Entity
{

  private $entity_path;
  private $config;
  private $errors = array();

  public function __construct($entity_path = false) {

    $this->config = Kohana::$config->load('config');
    $this->entity_path = $this->set_entity_path($entity_path);

  } //EOCo

  public static function factory($entity_path = false) {

    return new Entity($entity_path);

  } //EOM

  public function get_entity($conversion = true)
  {

    if ($this->entity_path == false) {
      return false;
    }
    $entity = $this->load_entity();
    if (isset($entity["delegate_entity_type"])) {
      $id = false;
      $slug = Session::instance()->get('slug');
      $config = Kohana::$config->load('config');
      $path = $config['dirs']['data'] . '/' . I18n::lang() . '/entities/' . $entity['delegate_entity_type'];
      $filesEntity = Dir::factory($path)->load();
      foreach($filesEntity as $fileEntity) {
        $dataEntity = File::factory($fileEntity)->load();
        if (isset($dataEntity['page_override']['slug']) && $dataEntity['page_override']['slug'] == $slug) {
          $id = $dataEntity['id'];
          foreach($dataEntity['page_override'] as $k => $v) {
            $entity[$k] = $v;
          }
        }
      }
      $entity["delegate_entity_id"] = $id;
    }
    if ($entity) {
      $entity = $this->apply_structure($entity);
    }
    if ($entity) {
      $entity = $this->apply_format_fields($entity, $conversion);
      ksort($entity);
      if ($entity["publish"]["value"] == false) {
        return false;
      }

      return $entity;
    }

    return false;

  } //EOM

  public function load_structure() {

    if ($this->entity_path == false) {
      return false;
    }
    if (isset($this->config['dirs']['data']) == false) {
      return false;
    }
    $type = $this->get_entity_type_from_path();
    $path = $this->config['dirs']['data'] . '/' . I18n::lang() . '/entities/' . $type . '/structure.json';
    $data = File::factory($path)->load();
    if ($data == false) {
      $this->set_error("danger", 'The structure of the "' . $type . '" entity does not exist.');
    }

    return $data;

  } //EOM

  public function load_entity() {

    if ($this->entity_path == false) {
      return false;
    }
    if (isset($this->config['dirs']['data']) == false) {
      return false;
    }
    $base_path = $this->config['dirs']['data'] . '/' . I18n::lang() . '/entities';
    $path = $base_path . '/' . $this->entity_path . '.json';
    $data = File::factory($path)->load();
    $path = $base_path . '/' . $this->entity_path . '.md';
    $data_content = File::factory($path)->load();
    if ($data_content) {
      $data['content'] = $data_content;
    }

    return $data;

  } //EOM

  public function set_error($type = false, $message = false) {

    if ($type == false) {
      return false;
    }
    if ($message == false) {
      return false;
    }

    $this->errors[] = array(
      "type" => $type,
      "message" => $message
    );

  } //EOM

  public function set_entity_path($entity_path = false)
  {
    if ($entity_path == false) {
      return false;
    }

    $this->entity_path = $entity_path;

    return $this->entity_path;

  } //EOM

  public function get_entity_path()
  {

    return $this->entity_path;

  } //EOM

  public function apply_structure($entity = false) {

    if ($entity == false) {
      return false;
    }
    $structure = $this->load_structure();
    if ($structure) {
      foreach($structure as $k => $v) {
        if (isset($entity[$k]) == false) {
          $entity[$k] = array(
            'value' => NULL,
            'structure' => $v
          );
        } else {
          $entity[$k] = array(
            'value' => $entity[$k],
            'structure' => $v
          );
        }
      }
      foreach($entity as $k => $v) {
        if (isset($structure[$k]) == false) {
          unset($entity[$k]);
        }
      }

      return $entity;
    }

    return false;

  } //EOM

  public function apply_format_fields($entity = false, $conversion = true) {

    if ($entity == false) {
      return false;
    }
    foreach($entity as $k => $v) {
      if (isset($v['structure']['type']) == false) {
        continue;
      }
      $utils_object = Utils::factory();
      $method = 'apply_format_fields_' . $v['structure']['type'];
      $method_render = 'apply_format_fields_' . $v['structure']['type'] . '_render';
      if (isset($v['structure']['default_value']) && empty($entity[$k]['value'])) {
        $entity[$k]['value'] = $v['structure']['default_value'];
      } else {
        if ($v['structure']['type'] == "content") {
          $entity[$k]['value'] = Utils::$method($v, $this->entity_path, $conversion);
          if (method_exists($utils_object, $method_render)) {
            $entity[$k]['render'] = Utils::$method_render($v, $this->entity_path, $conversion);
          }
        } else {
          $entity[$k]['value'] = Utils::$method($v, $this->entity_path);
          if (method_exists($utils_object, $method_render)) {
            $entity[$k]['render'] = Utils::$method_render($v, $this->entity_path);
          }
        }
      }
    }

    return $entity;

  } //EOM
  
  public static function search_entity_dir_from_id($id = false)
  {

    if ($id == false) {
      return false;
    }
    $config = Kohana::$config->load('config');
    $path = $config['dirs']['data'] . '/' . I18n::lang() . '/entities/page';
    $files = Dir::factory($path)->load();
    if (is_array($files)) {
      foreach($files as $file) {
        $filename = pathinfo($file, PATHINFO_FILENAME);
        $segments = explode('_', $filename);
        if (count($segments) == 1) {
          $data = File::factory($file)->load();
          if (isset($data['id']) && $data['id'] == $id) {
            return dirname($file);
          }
        }
      }
    }

    return false;

  } //EOM

  public static function search_entity_path_from_id($id = false)
  {

    if ($id == false) {
      return false;
    }
    $config = Kohana::$config->load('config');
    $path = $config['dirs']['data'] . '/' . I18n::lang() . '/entities';
    $files = Dir::factory($path)->load();
    if (is_array($files)) {
      foreach($files as $file) {
        $data = File::factory($file)->load();
        if (isset($data['id']) && $data['id'] == $id) {
          $filename = pathinfo($file, PATHINFO_FILENAME);
          $dirname = dirname($file);
          $dirnameItems = explode("/", $dirname);
          unset($dirnameItems[0]);
          unset($dirnameItems[1]);
          unset($dirnameItems[2]);
          $dirname = implode("/", $dirnameItems);
          $path = str_replace($config['dirs']['data'], "", $dirname) . '/' . $filename;
          $path = ltrim($path, "/");

          return $path;
        }
      }
    }
    /*
    if ($path == false) {
      $this->set_error("danger", 'No entities were found from this id:' . $id);
    }
    */

    return false;

  } //EOM

  public static function search_page_path_from_slug($slug = false, $lang = false)
  {
    if ($slug == false) {
      return false;
    }
    if ($lang == false) {
      $lang = I18n::lang();
    }
    $config = Kohana::$config->load('config');
    $path = $config['dirs']['data'] . '/' . $lang . '/entities/page';
    $files = Dir::factory($path)->load();
    if (is_array($files)) {
      foreach($files as $file) {
        $data = File::factory($file)->load();
        if (isset($data['slug']) && $data['slug'] == $slug) {
          $filename = pathinfo($file, PATHINFO_FILENAME);
          $dirname = dirname($file);
          $dirnameItems = explode("/", $dirname);
          unset($dirnameItems[0]);
          unset($dirnameItems[1]);
          unset($dirnameItems[2]);
          $dirname = implode("/", $dirnameItems);
          $path = str_replace($config['dirs']['data'], "", $dirname) . '/' . $filename;
          $path = ltrim($path, "/");
          return $path;
        } else {
          if (isset($data['delegate_entity_type']) && !is_array($data['delegate_entity_type'])) {
            // Search delegate entity with the slug
            $path = $config['dirs']['data'] . '/' . $lang . '/entities/' . $data['delegate_entity_type'];
            $filesEntity = Dir::factory($path)->load();
            foreach($filesEntity as $fileEntity) {
              $dataEntity = File::factory($fileEntity)->load();
              if (isset($dataEntity['page_override']['slug']) && $dataEntity['page_override']['slug'] == $slug) {
                $filename = pathinfo($file, PATHINFO_FILENAME);
                $dirname = dirname($file);
                $dirnameItems = explode("/", $dirname);
                unset($dirnameItems[0]);
                unset($dirnameItems[1]);
                unset($dirnameItems[2]);
                $dirname = implode("/", $dirnameItems);
                $path = str_replace($config['dirs']['data'], "", $dirname) . '/' . $filename;
                $path = ltrim($path, "/");
                return $path;
              }
            }
          }
        }
      }
    }
    return false;

  } //EOM

  public function get_entity_type_from_path() {

    if ($this->entity_path == false) {
      return false;
    }
    $pathParts = explode("/", $this->entity_path);

    return $pathParts[0];

  } //EOM

  public static function get_listing($type = false, $count = -1, $orderby = false, $order = false) {

    if ($type == false) {
      return false;
    }
    $config = Kohana::$config->load('config');
    $path = $config['dirs']['data'] . '/' . I18n::lang() . '/entities/' . $type;
    $files = Dir::factory($path)->load(array('md'));
    $listing = array();
    foreach($files as $file) {
      $filename = pathinfo($file, PATHINFO_FILENAME);
      $dirname = dirname($file);
      $dirnameItems = explode("/", $dirname);
      unset($dirnameItems[0]);
      unset($dirnameItems[1]);
      unset($dirnameItems[2]);
      $dirname = implode("/", $dirnameItems);
      $path = str_replace($config['dirs']['data'], "", $dirname) . '/' . $filename;
      $path = ltrim($path, "/");
      $entity = Entity::factory($path)->get_entity(false);
      if ($entity) {
        $listing[] = $entity;
      }
    }
    //Ordering
    if ($orderby && $order) {
      $listing = Entity::get_sorting_listing($listing, $orderby, $order);
    }

    //Count
    if ($count != -1) {
      $oldListing = $listing;
      $listing = array();
      $nb = 0;
      foreach($oldListing as $k => $v) {
        if ($nb < $count) {
          $listing[$k] = $v;
        }
        $nb++;
      }
    }

    return $listing;

  } //EOM

  public static function get_sorting_listing($listing = array(), $orderby = false, $order = false) {

    if (count($listing) == 0) {
      return false;
    }
    foreach($listing as $k => $v) {
      if (isset($v[$orderby]) == false) {
        continue;
      }
      switch($v[$orderby]['structure']['type']) {
        case 'date':
          $listing[$k]['listing_order'] = strtotime($v[$orderby]['value']);
          break;
        default:
          $listing[$k]['listing_order'] = $v[$orderby]['value'];
      }
    }
    if ($order == 'ASC') {
      usort($listing, function($a, $b) {
        if ($a == $b || isset($a['listing_order']) == false || isset($b['listing_order']) == false) {
          return 0;
        }
        return ($a['listing_order'] < $b['listing_order']) ? -1 : 1;
      });
    } else {
      usort($listing, function($a, $b) {
        if ($a == $b || isset($a['listing_order']) == false || isset($b['listing_order']) == false) {
          return 0;
        }
        return ($a['listing_order'] > $b['listing_order']) ? -1 : 1;
      });
    }

    return $listing;

  } //EOM

  public static function get_filters($type = false, $filter_type = false) {

    if ($type == false) {
      return false;
    }
    if ($type == false) {
      return false;
    }
    $config = Kohana::$config->load('config');
    $path = $config['dirs']['data'] . '/' . I18n::lang() . '/entities/' . $type;
    $files = Dir::factory($path)->load(array('md'));
    $filters = array();
    foreach($files as $file) {
      $filename = pathinfo($file, PATHINFO_FILENAME);
      $dirname = dirname($file);
      $dirnameItems = explode("/", $dirname);
      unset($dirnameItems[0]);
      unset($dirnameItems[1]);
      unset($dirnameItems[2]);
      $dirname = implode("/", $dirnameItems);
      $path = str_replace($config['dirs']['data'], "", $dirname) . '/' . $filename;
      $path = ltrim($path, "/");
      $entity = Entity::factory($path)->get_entity(false);
      if ($entity && isset($entity[$filter_type])) {
        $items = $entity[$filter_type]['value'];
        foreach($items as $value) {
          $filters[] = $value;
        }
      }
    }
    $filters = array_unique($filters);

    return $filters;

  } //EOM

} //EOC
