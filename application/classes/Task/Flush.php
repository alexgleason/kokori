<?php

class Task_Flush extends Minion_Task
{
  /**
   * Flush cache
   *
   * @return null
   */
  protected function _execute(array $params)
  {
    Cache::instance('memcache')->delete_all();

  }
}