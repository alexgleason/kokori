<?php

class Task_Operation extends Minion_Task
{
  /**
   * This is a demo task
   *
   * @return null
   */
  protected function _execute(array $params)
  {
    Operation::factory()->launch();

  }
}