<?php
class Model
{

  private $model_id;
  private $model_entity_path;
  private $config;

  public function __construct($model_id = false, $model_entity_path = false) {

    $this->config = Kohana::$config->load('config');
    $this->model_id = $this->set_model_id($model_id);
    $this->model_entity_path = $this->set_model_entity_path($model_entity_path);

  } //EOCo

  public static function factory($model_id = false, $model_entity_path = false) {

    return new Model($model_id, $model_entity_path);

  } //EOM

  public function get_model($conversion = true)
  {
    if ($this->get_model_entity_path() == false) {
      return false;
    }
    $model = $this->load_model();
    if ($model && $conversion) {
      $content = array(
        "value" => $model["content"]
      );
      $model["content"] = Utils::apply_format_fields_content($content, $this->model_entity_path, true,"model");

      return $model;
    }

    return false;

  } //EOM

  public function load_model() {

    if (isset($this->config['dirs']['data']) == false) {
      return false;
    }
    $base_path = $this->config['dirs']['data'] . '/' . I18n::lang() . '/models';
    $path = $base_path . '/' . $this->model_id . '.json';
    $data = File::factory($path)->load();
    if ($data) {
      $path = $base_path . '/' . $this->model_id . '.md';
      $data_content = File::factory($path)->load();
      if ($data_content) {
        $data['content'] = $data_content;
      }
    }
    return $data;

  } //EOM

  public function set_model_id($model_id = false)
  {
    if ($model_id == false) {
      return false;
    }

    $this->model_id = $model_id;

    return $this->model_id;

  } //EOM

  public function get_model_id()
  {

    return $this->model_id;

  } //EOM

  public function set_model_entity_path($model_entity_path = false)
  {
    if ($model_entity_path == false) {
      return false;
    }

    $this->model_entity_path = $model_entity_path;

    return $this->model_entity_path;

  } //EOM

  public function get_model_entity_path()
  {

    return $this->model_entity_path;

  } //EOM

} //EOC