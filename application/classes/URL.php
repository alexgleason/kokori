<?php
class URL extends Kohana_URL
{

  public static function search_redirection_from_slug($slug = false) {

    if ($slug == false) {
      return false;
    }
    $config = Kohana::$config->load('config');
    $path = $config['dirs']['data']  . '/' . I18n::lang() . '/entities/config/redirects.json';
    $redirects = File::factory($path)->load();
    if (isset($redirects[$slug])) {
      $redirect = $redirects[$slug];
      if (isset($redirect["slug"]) == false || $redirect["slug"] == $slug) {
        return false;
      }
      // Check reverse redirection
      if (isset($redirects[$redirect["slug"]]) &&  $redirects[$redirect["slug"]]["slug"] == $slug) {
        return false;
      }
      if (isset($redirect["lang"]) == false) {
        $lang = I18n::lang();
      } else {
        $lang = $redirect["lang"];
      }
      return $lang . "/" . $redirect["slug"];
    }

    return false;

  } //EOM

  public static function search_relationship_from_slug($slug = false) {

    if ($slug == false) {
      return false;
    }
    $config = Kohana::$config->load('config');
    $context = Session::instance()->get('context');
    $entity = $context["entity"];
    $elements = array();
    $elements[] = $entity;
    while(isset($entity["parent"]) && $entity["parent"]["value"]) {
      $entityPath = Entity::search_page_path_from_slug($entity["parent"]["value"]);
      $entity = false;
      if ($entityPath) {
        $entity = Entity::factory($entityPath)->get_entity();
        if ($entity) {
          $elements[] = $entity;
        }
      }
    }
    krsort($elements);

    return $elements;

  } //EOM

  public static function search_parent_from_slug($slug = false) {

    if ($slug == false) {
      return false;
    }
    $config = Kohana::$config->load('config');

    return false;

  } //EOM

} //EOC
