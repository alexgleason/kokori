<?php

class Controller_Tools extends Controller_Template
{

  public $template = "tools/template";
  public $params = array();
  public $template_data = array();
  public $action = false;
  public $config;

  public function before()
  {

    parent::before();
    $this->config = Kohana::$config->load('config');
    $this->action = $this->request->action();
    I18n::lang('en');

  } //EOM

  public function after()
  {

    $content = View::factory('tools/' . $this->action, array('data' => $this->template_data));
    $this->template->content = $content;
    $this->template->data = $this->template_data;

    parent::after();

  } //EOM

  public function action_index() {

  }


  public function action_preview() {

  }

} //EOC
