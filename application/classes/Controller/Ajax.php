<?php

class Controller_Ajax extends Controller
{

  public $action = false;
  public $template_data = array();
  public $config;

  public function before()
  {

    parent::before();
    $this->config = Kohana::$config->load('config');
    $language = Arr::get($_POST, 'language');
    if ($language) {
      I18n::lang($language);
    } else {
      I18n::lang($this->config['default_language']);
    }
    $this->action = $this->request->action();

  } //EOM

  public function after()
  {

    echo json_encode($this->template_data);

    parent::after();

  } //EOM

  public function action_index()
  {

  }

  public function action_tools_get_preview()
  {

    $content = Arr::get($_POST, 'content', false);
    if ($content) {
      $content = Utils::convert_content($content);
      $this->template_data['content'] = $content;
    }
  } //EOM

  public function action_modal()
  {

    $action = Arr::get($_POST, 'action', false);
    $method = 'action_modal_' . $action;
    if (method_exists($this, $method)) {
      $data = $this->{$method}();
    }
    echo json_encode($data);
    exit();

  } //EOM

  public function action_modal_download()
  {

    $id = Arr::get($_POST, 'id', false);
    $title = Arr::get($_POST, 'title', false);
    $buttonLabel = Arr::get($_POST, 'buttonLabel', false);
    $file = Arr::get($_POST, 'file', false);
    $category = Arr::get($_POST, 'category', false);
    $entity_path = Entity::search_entity_path_from_id($id);
    $entity = Entity::factory($entity_path)->get_entity(true);
    if ($id == 'downloads') {
      $file_path = $this->config['dirs']['data'] . '/' . I18n::lang() . '/downloads/config.json';
      $content = File::factory($file_path)->load();
      $content = array(
        'value' => $content['downloads'][$category]
      );
      $downloads = Utils::apply_format_fields_downloads($content);
      $download = $downloads['default'];
      if (isset($downloads['others'][$file])) {
        $download = $downloads['others'][$file];
      }
    } else {
      if ($file == 'default') {
        $download = $entity['downloads']['value']['default'];
      } else {
        if (isset($entity['downloads']['value']['others'][$file])) {
          $download = $entity['downloads']['value']['others'][$file];
        }
      }
    }
    // Size
    if (isset($download['file']) && !empty($download['file']) && (!isset($download['size']) || empty($download['size']))) {

      try // On va essayer d'effectuer les instructions situées dans ce bloc.
      {
        $download['size'] = filesize($download['file']);
      } catch (Exception $e) // On va attraper les exceptions "Exception" s'il y en a une qui est levée.
      {

      }
    }
    if (isset($download['size']) && !empty($download['size'])) {
      $download['size'] = Utils::getSize($download['size']);
    }
    $view = View::factory('front/modals/download');
    $view->bind('id', $id);
    $view->bind('entity', $entity);
    $view->bind('title', $title);
    $view->bind('buttonLabel', $buttonLabel);
    $view->bind('download', $download);
    $content = $view->render();
    $data = array(
      'content' => $content
    );

    return $data;

  } //EOM

} //EOC
