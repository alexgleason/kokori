<?php

class Operation
{

  private $config;

  public function __construct()
  {

    $this->config = Kohana::$config->load('config');

  } //EOCo

  public static function factory()
  {
    return new Operation();

  } //EOM

  public function launch() {

    $path = $this->config['dirs']['data'] . '/operations';
    $operationsAlreadyExecuted = File::factory($path . '/operations.json')->load();
    if (empty($operationsAlreadyExecuted)) {
      $operationsAlreadyExecuted = array();
    }
    $files = Dir::factory($path)->load();
    foreach($files as $k => $file) {
      if (basename($file) == 'operations.json') {
        continue;
      }
      $operations = File::factory($file)->load();
      if (in_array($operations['id'], $operationsAlreadyExecuted)) {
        continue;
      }
      foreach($operations['operations'] as $action => $operation) {
        $method = 'launch_operation_' . $action;
        if (method_exists($this, $method)) {
          $this->{$method}($operation);
        }
      }
      $operationsAlreadyExecuted[] = $operations['id'];

    }
    $operationsAlreadyExecuted = json_encode($operationsAlreadyExecuted, JSON_PRETTY_PRINT);
    file_put_contents($path . '/operations.json', $operationsAlreadyExecuted);


  } //EOM

  public function launch_operation_flush_cache($operation = array()) {

    if (isset($operation['values']) == false || is_array($operation['values']) == false) {
      return false;
    }
    echo 'ok';
    foreach($operation['values'] as $value) {
      if ($value == 'all') {
        Cache::instance('memcache')->delete_all();
      } else {
        Cache::instance('memcache')->delete($value);
      }
    }

  } //EOM


} //EOC
