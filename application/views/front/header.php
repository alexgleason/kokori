<header class="<?php if (Utils::is_home()):?>header-transparent<?php endif;?>">
  <div class="container">
    <div class="logo">
      <a href="/<?php echo I18n::lang();?>" title="">
        <?php echo HTML::get_logo();?>
      </a>
    </div>
    <button class="btn btn-primary btn-menu d-lg-none" type="button" data-toggle="collapse" data-target="#menu-header">
      <i class="fa fa-bars"></i>
    </button>
    <?php if (isset($data['entry'])):?>
      <?php echo Widget::factory('menu')->get_widget(array('id' => 'header', 'slug' => $data['entry']['entity']['slug']['value']));?>
    <?php else:?>
      <?php echo Widget::factory('menu')->get_widget(array('id' => 'header', 'slug' => false));?>
    <?php endif;?>
    <?php echo Widget::factory('languages')->get_widget(array('languages' => $data['config']['languages']));?>
  </div>
</header>