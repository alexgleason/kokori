<div class="widget widget-languages dropdown">
  <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo $data['languages'][$data['language']]["short_name"];?>
  </a>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
    <?php foreach($data['languages'] as $k => $v):?>
      <a class="dropdown-item<?php if ($k != $data['language']):?> current<?php endif;?>" href="<?php echo $v['url'];?>"><?php echo $v['name'];?></a>
    <?php endforeach;?>
  </div>
</div