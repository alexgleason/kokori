<?php if ($data['menu']):?>
  <div class="widget widget-menu">
    <?php echo HTML::get_menu($data['id'], $data['menu']);?>
  </div>
<?php endif;?>