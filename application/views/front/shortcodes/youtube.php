<div class="shortcode shortcode-youtube<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <iframe
    src="https://www.youtube.com/embed/<?php echo $data['atts']['id'];?>"
    allow="encrypted-media"
    <?php if ($data['atts']['allowfullscreen']):?>
        allowfullscreen
    <?php endif;?>>
  </iframe>
</div>