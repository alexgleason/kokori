<div class="shortcode shortcode-game-featured<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <div class="row">
    <div class="col-md-6">
      <a href="<?php echo $data["entity"]["page_slug"]["value"];?>" title="<?php echo $data["entity"]["title"]["value"];?>" class="thumbnail">
        <img alt="<?php echo $data["entity"]["title"]["value"];?>" src="<?php echo $data["entity"]["image_thumbnail"]["value"];?>"/>
      </a>
    </div>
    <div class="col-md-6">
      <?php if ($data["atts"]["title"]):?>
        <h2><?php echo $data["atts"]["title"];?></h2>
        <hr/>
      <?php endif;?>
      <h3><?php echo $data["entity"]["title"]["render"];?></h3>
      <div class="metas">
        <span>
          <?php echo $data["entity"]["authors"]["render"];?>
        </span>
        <span>
          <?php echo $data["entity"]["release_date"]["render"];?>
        </span>
        <span>
          <?php echo $data["entity"]["types"]["render"];?>
        </span>
      </div>

      <p><?php echo $data["entity"]["excerpt"]["render"];?></p>
      <div class="more-infos">
        <?php if ($data["atts"]["button-label"]):?>
          <a class="btn btn-primary" title="<?php echo $data["atts"]["button-label"];?>" href="<?php echo $data["entity"]["page_slug"]["value"];?>"><?php echo $data["atts"]["button-label"];?></a>
        <?php endif;?>
      </div>
    </div>
  </div>
</div>