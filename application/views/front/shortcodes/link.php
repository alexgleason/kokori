<div class="shortcode shortcode-inline shortcode-link<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <a title="<?php echo $data['atts']['title'];?>" href="<?php echo $data['atts']['url'];?>" target="<?php echo $data['atts']['target'];?>">
    <?php if ($data['atts']['icon']):?>
      <?php echo $data['atts']['icon'];?>
    <?php endif;?>
    <?php echo $data['atts']['label'];?>
  </a>
</div>
