<div class="shortcode shortcode-article-listing<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <div data-toggle="isotope" class="row">
    <?php foreach($data['listing'] as $entity):?>
      <div class="col-md-<?php echo $data['atts']['column-width'];?> <?php echo Utils::get_class_filters($entity);?>">
        <div class="item">
          <span class="alpha d-none"><?php echo $entity["title"]["value"];?></span>
          <span class="date d-none"><?php echo strtotime($entity["creation_date"]["value"]);?></span>
          <a href="<?php echo $entity["page_slug"]["value"];?>" title="<?php echo $entity["title"]["value"];?>">
            <div class="image">
              <img class="img-fluid" alt="<?php echo $entity["title"]["value"];?>" src="<?php echo $entity["image_thumbnail"]["value"];?>"/>
            </div>
            <div class="details">
              <h2 class="title"><?php echo $entity["title"]["value"];?></h2>
              <div class="date">
                <i class="fa fa-calendar-alt"></i>
                <?php echo $entity["creation_date"]["render"];?>
              </div>
              <div class="excerpt">
                <?php echo $entity["excerpt"]["render"];?>
              </div>
            </div>
          </a>
          <div class="categories">
            <?php echo $entity["categories"]["render"];?>
          </div>
        </div>
      </div>
    <?php endforeach;?>
    <?php if ($data['atts']['message-empty']):?>
      <div class="col-md-12 none">
        <div class="alert alert-primary">
          <?php echo $data['atts']['message-empty'];?>
        </div>
      </div>
    <?php endif;?>
  </div>
</div>