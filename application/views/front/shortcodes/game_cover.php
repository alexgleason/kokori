<div class="shortcode shortcode-game-cover<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <div data-toggle="parallax"  data-paroller-factor="0.3" data-paroller-type="background" style="background-image:url(<?php echo $data["entity"]["image_cover"]["value"];?>);">
    <img alt="<?php echo $data["entity"]["title"]["value"];?>" src="<?php echo $data["entity"]["image_logo"]["value"];?>"/>
  </div>
 </div>