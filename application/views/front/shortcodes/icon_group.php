<div class="shortcode shortcode-icon-group<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>">
  <span class="fa-layers fa-fw fa-<?php echo $data['atts']['size'];?>">
    <?php echo $data['content'];?>
  </span>
</div>