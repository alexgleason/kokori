<div class="shortcode shortcode-align shortcode-align-<?php echo $data['atts']['type'];?><?php if ($data['atts']['effect-in']): ?> effect-in<?php endif; ?>"<?php if ($data['atts']['effect-in']): ?> data-effect-in="<?php echo $data['atts']['effect-in']; ?>"<?php endif; ?>>
  <?php echo $data['content']; ?>
</div>