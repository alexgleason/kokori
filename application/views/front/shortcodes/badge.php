<div class="shortcode shortcode-inline shortcode-badge<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>">
  <span class="badge badge-<?php echo $data['atts']['type'];?>">
    <?php if ($data['atts']['icon']):?>
      <?php echo $data['atts']['icon'];?>
    <?php endif;?>
    <?php echo $data['atts']['label'];?>
  </span>
</div>