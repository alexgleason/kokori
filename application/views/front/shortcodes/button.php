<div class="shortcode shortcode-inline shortcode-button<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>">
  <?php if ($data['atts']['url']):?>
    <a class="btn<?php if ($data['atts']['outline']):?> btn-outline-<?php echo $data['atts']['type'];?><?php else:?> btn-<?php echo $data['atts']['type'];?><?php endif;?><?php if($data['atts']['size']):?> btn-<?php echo $data['atts']['size'];?><?php endif;?>" href="<?php echo $data['atts']['url'];?>" target="<?php echo $data['atts']['target'];?>">
      <?php if ($data['atts']['icon']):?>
        <?php echo $data['atts']['icon'];?>
      <?php endif;?>
      <?php echo $data['atts']['label'];?>
    </a>
  <?php else:?>
    <button class="btn<?php if ($data['atts']['outline']):?> btn-outline-<?php echo $data['atts']['type'];?><?php else:?> btn-<?php echo $data['atts']['type'];?><?php endif;?><?php if($data['atts']['size']):?> btn-<?php echo $data['atts']['size'];?><?php endif;?>">
      <?php if ($data['atts']['icon']):?>
        <?php echo $data['atts']['icon'];?>
      <?php endif;?>
      <?php echo $data['atts']['label'];?>
    </button>
  <?php endif;?>
</div>
