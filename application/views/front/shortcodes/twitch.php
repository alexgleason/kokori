<div class="shortcode shortcode-twitch<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <iframe
    src="http://player.twitch.tv?video=<?php echo $data['atts']['id'];?>"
    <?php if ($data['atts']['allowfullscreen']):?>
    allowfullscreen="<allowfullscreen>">
    <?php endif;?>
  </iframe>
</div>