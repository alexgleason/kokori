<?php if (!empty($data["entity"]["image_logo_author"]["value"])):?>
  <div class="shortcode shortcode-inline shortcode-game-logo-author<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
    <img alt="<?php echo $data["entity"]["title"]["value"];?>" src="<?php echo $data["entity"]["image_logo_author"]["value"];?>"/>
  </div>
<?php endif;?>