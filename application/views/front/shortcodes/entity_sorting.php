<div class="shortcode shortcode-entity-filter<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
<h3><?php echo $data['atts']['title'];?></h3>
  <div data-toggle="sorting">
    <label class="control control-radio"><?php echo $data['atts']['label-sorting-alpha'];?>
      <input name="sorting" data-ascending="asc" id="sorting-alpha" type="radio" value="alpha" checked="checked"/>
      <div class="control-indicator"></div>
    </label>
    <label class="control control-radio"><?php echo $data['atts']['label-sorting-date'];?>
      <input name="sorting" data-ascending="desc" id="sorting-date" type="radio" value="date"/>
      <div class="control-indicator"></div>
    </label>
  </div>
</div>