<div class="shortcode shortcode-entity-filter<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <h3><?php echo $data['atts']['title'];?></h3>
  <div data-type="<?php echo $data['atts']['filter'];?>" data-toggle="filter">
    <?php foreach($data['filters'] as $filter):?>
      <label class="control control-checkbox">
        <?php echo $filter;?>
        <input <?php if (Utils::check_filter_url($data['atts']['filter'], Utils::slugify($filter))):?>"<?php endif;?>id="<?php echo $data['atts']['filter'] . '-' . Utils::slugify($filter);?>" class="form-check-input" type="checkbox" value="<?php echo Utils::slugify($filter);?>"/>
        <div class="control-indicator"></div>
      </label>
    <?php endforeach;?>
  </div>
</div>