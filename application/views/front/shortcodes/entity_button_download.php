<?php $has_multiple_downloads = count($data['entity']['downloads']['value']['others']) > 1; ?>
<div class="shortcode shortcode-inline shortcode-entity-button-download<?php if (!$data['entity']['downloads']['value']):?> disabled<?php endif;?><?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <button <?php if ($data['entity']['downloads']['value']):?>data-toggle="download" data-file="default" data-id="<?php echo $data['entity']['id']['value'];?>"<?php endif;?> data-modal-title="<?php echo $data['atts']['modal-title'];?>" data-modal-button-label="<?php echo $data['atts']['modal-button-label'];?>" data-language="<?php echo I18n::lang();?>" type="button" class="btn btn-primary <?php if ($has_multiple_downloads):?>with-dropdown<?php endif;?>" aria-haspopup="true" aria-expanded="false">
      <div class="icon">
        <?php if ($data['entity']['downloads']['value']):?>
        <i class="fa fa-download"></i>
        <?php else:?>
          <i class="fa fa-calendar-alt"></i>
        <?php endif;?>
      </div>
    <div class="label">
      <?php if ($data['entity']['downloads']['value']):?>
        <?php echo $data['atts']['label'];?>
      <?php else:?>
        <?php echo $data['atts']['label-disabled'];?>
      <?php endif;?>
      <?php if (isset($data['entity']['downloads']['value']) && count($data['entity']['downloads']['value']) > 1):?>
        <small><?php echo 'Version '; echo $data['entity']['version']['value'];?></small>
      <?php endif;?>
    </div>
  </button>
  <?php if ($has_multiple_downloads):?>
    <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown"></button>
    <div class="dropdown-menu">
      <?php foreach($data['entity']['downloads']['value']['others'] as $k => $v):?>
        <a data-toggle="download" data-file="<?php echo $k;?>" data-id="<?php echo $data['entity']['id']['value'];?>" data-modal-title="<?php echo $data['atts']['modal-title'];?>" data-modal-button-label="<?php echo $data['atts']['modal-button-label'];?>" data-language="<?php echo I18n::lang();?>" class="dropdown-item" href="#"><?php echo $v["label"];?></a>
      <?php endforeach;?>
    </div>
  <?php endif;?>
</div>