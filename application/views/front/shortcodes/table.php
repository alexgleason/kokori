<div class="shortcode shortcode-table<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <table class="table">
    <tbody>
      <?php echo $data['content'];?>
    </tbody>
  </table>
</div>