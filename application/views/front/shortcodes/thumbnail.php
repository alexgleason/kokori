<div class="shortcode shortcode-game-thumbnail<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <a href="<?php echo $data["entity"]["page_slug"]["value"];?>" title="<?php echo $data["entity"]["title"]["value"];?>">
    <img alt="<?php echo $data["entity"]["title"]["value"];?>" src="<?php echo $data["entity"]["image_thumbnail"]["value"];?>"/>
  </a>
</div>