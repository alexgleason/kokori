<div class="shortcode shortcode-entity-search<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <label class="sr-only" for="entity-search">Username</label>
  <div class="input-group mb-2">
    <div class="input-group-prepend">
      <div class="input-group-text">
        <i class="fas fa-search"></i>
      </div>
    </div>
    <input data-toggle="search" value="<?php echo Arr::get($_GET, 'search');?>" type="text" class="form-control" id="entity-search"<?php if ($data['atts']['text-placeholder']):?> placeholder="<?php echo $data['atts']['text-placeholder'];?>"<?php endif;?>>
  </div>
</div>