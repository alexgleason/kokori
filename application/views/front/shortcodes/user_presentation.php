<div class="shortcode shortcode-user-presentation<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <div class="row">
    <div class="col-md-3">
      <div class="image">
        <img alt="<?php echo $data['atts']['name'];?>" src="<?php echo $data['atts']['image'];?>"/>
      </div>
    </div>
    <div class="col-md-9">
      <h3><?php echo $data['atts']['name'];?></h3>
      <p><?php echo $data['atts']['description'];?></p>
      <?php if ($data['atts']['link-label'] && $data['atts']['link-url'] && $data['atts']['link-title'] && $data['atts']['link-target']):?>
      <a href="<?php echo $data['atts']['link-url'];?>" target="<?php echo $data['atts']['link-target'];?>" title="<?php echo $data['atts']['link-title'];?>"><?php echo $data['atts']['link-label'];?></a>
      <?php endif;?>
    </div>
  </div>
</div>