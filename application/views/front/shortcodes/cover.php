<div<?php if ($data['atts']['is-parallax']):?> data-toggle="parallax" data-paroller-factor="0.3" data-paroller-type="background"<?php endif;?> class="shortcode shortcode-cover<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <div class="container">
    <?php echo $data['content'];?>
  </div>
</div>