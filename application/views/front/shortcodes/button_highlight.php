<div class="shortcode shortcode-button-highlight<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>">
  <?php if ($data['atts']['url']):?>
    <a href="<?php echo $data['atts']['url'];?>" target="<?php echo $data['atts']['target'];?>">
      <div class="icon">
        <?php if ($data['atts']['icon']):?>
          <?php echo HTML::get_theme_image("icons/" . $data['atts']['icon'] . ".svg");?>
        <?php endif;?>
      </div>
      <div class="infos">
        <?php if ($data['atts']['subtitle']):?>
          <div class="subtitle">
            <?php echo $data['atts']['subtitle'];?>
          </div>
        <?php endif;?>
      <div>
        <?php echo $data['atts']['label'];?>
      </div>
      </div>
    </a>
  <?php else:?>
    <button>
      <div class="icon">
        <?php if ($data['atts']['icon']):?>
          <?php echo HTML::get_theme_image("icons/" . $data['atts']['icon'] . ".svg");?>
        <?php endif;?>
      </div>
      <div class="infos">
        <?php if ($data['atts']['subtitle']):?>
          <div class="subtitle">
            <?php echo $data['atts']['subtitle'];?>
          </div>
        <?php endif;?>
        <div>
          <?php echo $data['atts']['label'];?>
        </div>
      </div>
    </button>
      </div>
  <?php endif;?>
<div class="clearfix"></div>
</div>
