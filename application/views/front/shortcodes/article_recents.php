<div class="shortcode shortcode-article-listing<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <h2><?php echo $data['atts']['title'];?></h2>
  <div class="listing">
    <ul>
      <?php foreach($data['listing'] as $entity):?>
        <li>
          <a href="<?php echo $entity["page_slug"]["value"];?>" title="<?php echo $entity["title"]["value"];?>">
            <?php echo $entity["title"]["value"];?></h2>
            <div class="listing-item-meta">
              <div class="date">
                <i class="fa fa-calendar-alt"></i>
                <?php echo $entity["creation_date"]["value"];?></h2>
              </div>
            </div>
          </a>
        </li>
      <?php endforeach;?>
    </ul>
  </div>
</div>