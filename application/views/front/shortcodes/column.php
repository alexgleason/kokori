<div class="shortcode shortcode-column <?php if ($data['atts']['width'] == "auto"):?>col-12 col-md<?php else:?>col-12 col-md-<?php echo $data['atts']['width'];?><?php endif;?><?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <?php echo $data['content'];?>
</div>