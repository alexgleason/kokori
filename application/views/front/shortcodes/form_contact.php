<div class="shortcode shortcode-form-contact<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>">
<?php if (isset($data['errors'])):?>
  <?php if (count($data['errors']) > 0):?>
    <div class="alert alert-danger"><?php echo $data['atts']['message-error'];?></div>
  <?php else:?>
    <div class="alert alert-success"><?php echo $data['atts']['message-success'];?></div>
  <?php endif;?>
<?php endif;?>
<form id="form-contact" method="POST">
  <input type="hidden" name="action" value="form_contact"/>
  <div class="form-group">
    <input value="<?php echo Arr::get($_POST, 'name');?>" name="name" type="name" class="form-control<?php if (isset($data['errors']['name'])):?> error<?php endif?>"  placeholder="<?php echo $data['atts']['name-label'];?>">
  </div>
  <div class="form-group">
    <input value="<?php echo Arr::get($_POST, 'email');?>" name="email" type="email" class="form-control<?php if (isset($data['errors']['email'])):?> error<?php endif?>" placeholder="<?php echo $data['atts']['email-label'];?>">
  </div>
  <div class="form-group">
    <input value="<?php echo Arr::get($_POST, 'subject');?>" name="subject" type="subject" class="form-control<?php if (isset($data['errors']['subject'])):?> error<?php endif?>"  placeholder="<?php echo $data['atts']['subject-label'];?>">
  </div>
  <div class="form-group">
    <textarea name="message" class="form-control<?php if (isset($data['errors']['message'])):?> error<?php endif?>"  placeholder="<?php echo $data['atts']['message-label'];?>"><?php echo Arr::get($_POST, 'message');?></textarea>
  </div>
  <div class="actions">
    <button type="submit" data-sitekey="<?php echo $data['atts']['captcha-sitekey'];?>" data-callback='validateCaptcha' class="btn btn-primary g-recaptcha"><?php echo $data['atts']['button-label'];?></button>
  </div>
</form>
</div>