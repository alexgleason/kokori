<div class="shortcode shortcode-card bg-<?php echo $data['atts']['type'];?><?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>">
  <div class="card">
    <?php if($data['atts']['title']):?>
      <div class="card-header">
          <?php echo $data['atts']['title'];?>
      </div>
    <?php endif;?>
      <?php if($data['content']):?>
        <div class="card-body">
          <?php echo $data['content'];?>
        </div>
      <?php endif;?>
  </div>
</div>
