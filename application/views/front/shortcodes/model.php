<div class="shortcode shortcode-model shortcode-model-<?php echo $data['atts']['id'];?><?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>">
  <?php echo $data['content'];?>
</div>
