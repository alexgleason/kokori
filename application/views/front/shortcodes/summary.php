<div class="shortcode shortcode-summary<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <div class="list-group">
    <?php foreach($data['summary'] as $element):?>
      <a data-toggle="heading-scroll" data-index="<?php echo $element["index"];?>" data-level="<?php echo $element["level"];?>"" class="list-group-item list-group-item-action list-group-item-level-<?php echo $element['level'];?>" style="padding-left: <?php echo 10 * $element['level'] - 1;?>px;" href="#">
        <?php echo $element['label'];?>
      </a>
    <?php endforeach;?>
  </div>
</div>