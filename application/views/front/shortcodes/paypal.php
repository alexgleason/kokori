<div class="shortcode shortcode-paypal<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>">
  <h3>
    <?php if ($data['atts']['icon']):?>
      <?php echo $data['atts']['icon'];?>
    <?php endif;?>
    <?php echo $data['atts']['title'];?>
  </h3>
  <p>
    <?php echo $data['content'];?>
  </p>
  <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
    <input type="hidden" name="business" value="<?php echo $data['atts']['email'];?>">
    <input type="hidden" name="cmd" value="_donations">
    <input type="hidden" name="item_name" value="<?php echo $data['atts']['name'];?>">
    <input type="hidden" name="item_number" value="<?php echo $data['atts']['subject'];?>">
    <input type="hidden" name="currency_code" value="<?php echo $data['atts']['currency'];?>">
    <div class="actions">
      <button type="submit" class="btn btn-<?php echo $data['atts']['type'];?>">
        <?php echo $data['atts']['label'];?>
      </button>
    </div>
    <img alt="<?php echo $data['atts']['label'];?>" width="1" height="1" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >
  </form>
</div>