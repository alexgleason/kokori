<div class="shortcode shortcode-inline shortcode-game-icon<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <img alt="<?php echo $data["entity"]["title"]["value"];?>" src="<?php echo $data["entity"]["icons"]["value"][$data['atts']['size']];?>"/>
</div>