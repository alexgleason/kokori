<div class="shortcode shortcode-article-metas<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <span class="date">
    <i class="fa fa-calendar-alt"></i>
    <?php echo $data["entity"]["creation_date"]["render"];?>
  </span>
  <span class="categories">
    <?php echo $data["entity"]["categories"]["render"];?>
  </span>
</div>