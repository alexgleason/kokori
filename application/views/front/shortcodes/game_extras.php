<?php if ($data['entity']['extras']['value']):?>
<div class="shortcode shortcode-game-extras<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <h2>Extras</h2>
  <?php foreach($data['entity']['extras']['value'] as $extra):?>
    <div class="extra">
      <?php if ($extra["type"] == "link"):?>
        <a title="<?php echo $extra['title'];?>" target="<?php echo $extra['target'];?>" href="<?php echo $extra['url'];?>">
          <?php if ($extra['icon']):?>
            <i class="<?php echo $extra['icon_category'];?> fa-<?php echo $extra['icon'];?>"></i>
          <?php endif;?>
          <?php echo $extra['label'];?>
        </a>
      <?php endif;?>
    </div>
  <?php endforeach;?>
</div>
<?php endif;?>