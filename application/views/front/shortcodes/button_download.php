<?php $has_multiple_downloads = count($data['downloads']['others']) > 1; ?>
<div class="shortcode shortcode-inline shortcode-button-download<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <button <?php if ($data['downloads']):?>data-toggle="download" data-file="default" data-id="downloads" data-category="<?php echo $data['atts']['category'];?>"<?php endif;?> data-modal-title="<?php echo $data['atts']['modal-title'];?>" data-modal-button-label="<?php echo $data['atts']['modal-button-label'];?>" data-language="<?php echo I18n::lang();?>" type="button" class="btn<?php if ($data['atts']['outline']):?> btn-outline-<?php echo $data['atts']['type'];?><?php else:?> btn-<?php echo $data['atts']['type'];?><?php endif;?><?php if($data['atts']['size']):?> btn-<?php echo $data['atts']['size'];?><?php endif;?><?php if ($has_multiple_downloads):?> with-dropdown<?php endif;?>" aria-haspopup="true" aria-expanded="false">
    <div class="icon">
      <?php if ($data['atts']['icon']):?>
        <?php echo $data['atts']['icon'];?>
      <?php endif;?>
    </div>
    <div class="label">
      <?php echo $data['atts']['label'];?>
      <?php if (isset($data['downloads']['others']) && count($data['downloads']['others']) > 0):?>
        <small><?php echo $data['downloads']['default']['version'];?> - <?php echo $data['downloads']['default']['platform'];?></small>
      <?php endif;?>
    </div>
  </button>
  <?php if ($has_multiple_downloads):?>
    <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown"></button>
    <div class="dropdown-menu">
      <?php foreach($data['downloads']['others'] as $k => $v):?>
        <a data-toggle="download" data-file="<?php echo $k;?>" data-id="downloads" data-category="<?php echo $data['atts']['category'];?>" data-modal-title="<?php echo $data['atts']['modal-title'];?>" data-modal-button-label="<?php echo $data['atts']['modal-button-label'];?>" data-language="<?php echo I18n::lang();?>" class="dropdown-item" href="#"><?php echo $v["label"];?></a>
      <?php endforeach;?>
    </div>
  <?php endif;?>
</div>
