<div class="shortcode shortcode-container<?php if ($data['atts']['layout']):?> container-<?php echo $data['atts']['layout'];?><?php else:?> container<?php endif;?><?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <?php echo $data['content'];?>
</div>