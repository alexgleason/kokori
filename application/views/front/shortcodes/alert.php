<div class="shortcode shortcode-alert<?php if ($data['atts']['effect-in']): ?> effect-in<?php endif; ?>"<?php if ($data['atts']['effect-in']): ?> data-effect-in="<?php echo $data['atts']['effect-in']; ?>"<?php endif; ?>>
  <div class="alert alert-<?php echo $data['atts']['type']; ?>" role="alert">
    <?php if ($data['atts']['dismiss']): ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    <?php endif; ?>
    <?php if ($data['atts']['title'] || $data['atts']['icon']): ?>
      <h3>
        <?php if ($data['atts']['icon']): ?>
          <?php echo $data['atts']['icon'];?>
        <?php endif; ?>
        <?php if ($data['atts']['title']): ?>
          <?php echo $data['atts']['title'];?>
        <?php endif;?>
      </h3>
    <?php endif; ?>
    <?php echo $data['content']; ?>
  </div>
</div>