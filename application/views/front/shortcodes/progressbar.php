<div class="shortcode shortcode-progressbar<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>"">
  <div class="progress" style="height: <?php echo $data['atts']['height'];?>px;">
    <div
      style="width: <?php echo $data['atts']['width'];?>%;"
      class="progress-bar<?php if($data['atts']['striped']):?> progress-bar-striped<?php endif;?><?php if($data['atts']['animated']):?> progress-bar-animated<?php endif;?> bg-<?php echo $data['atts']['type'];?>"
      role="progressbar"
      aria-valuenow="0"
      aria-valuemin="0"
      aria-valuemax="100"><?php echo $data['content'];?></div>
  </div>
</div>