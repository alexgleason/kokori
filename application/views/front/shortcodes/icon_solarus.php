<div style="width: <?php echo  $data['atts']['size'];?>px;" class="shortcode shortcode-inline shortcode-icon-solarus<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <?php echo HTML::get_theme_image("icons/" . $data['atts']['icon'] . ".svg");?>
</div>