<?php if (empty($data['content']) == false):?>
  <<?php if ($data['atts']['is-header']):?>th<?php else:?>td<?php endif;?> class="shortcode shortcode-table-column<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?><?php if ($data['atts']['colspan']):?> colspan="<?php echo $data['atts']["colspan"];?>"<?php endif;?><?php if ($data['atts']['rowspan']):?> rowspan="<?php echo $data['atts']["rowspan"];?>"<?php endif;?>>
    <?php echo $data['content'];?>
  </<?php if ($data['atts']['is-header']):?>th<?php else:?>td<?php endif;?>>
<?php endif;?>