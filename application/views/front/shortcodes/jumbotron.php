<div class="shortcode shortcode-jumbotron<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <div class="jumbotron">
    <h1><?php echo $data['atts']['title'];?></h1>
    <p class="lead"><?php echo $data['atts']['lead'];?></p>
    <?php if($data['content']):?>
        <?php echo $data['content'];?>
    <?php endif;?>
  </div>
</div>