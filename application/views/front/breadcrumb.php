<div class="breadcrumb-container">
  <div class="container">
    <?php if (isset($data['entry']['entity']['slug']['value'])):?>
      <?php echo Widget::factory('breadcrumb')->get_widget($data['entry']['entity']['slug']['value']);?>
    <?php endif;?>
  </div>
</div>