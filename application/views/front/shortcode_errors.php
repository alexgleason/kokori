<div class="shortcode shortcode-errors">
  <div class="alert alert-danger text-center">
    <h3>[/<?php echo $data["name"];?>]</h3>
    <hr/>
    <div>
      <small>
        <?php if (count($errors) > 1):?>
          <?php echo count($errors);?> errors found
        <?php else:?>
          <?php echo count($errors);?> error found
        <?php endif;?>
      </small>
    </div>
    <hr/>
    <?php foreach($errors as $message):?>
      <div class="mt-4">
        <div>
          <small>
            <?php echo $message['message'];?>
          </small>
        </div>
        <div class="mt-4">
          <small>
            <strong>
              File concerned:
              <?php if ($message['file']):?>
                <?php echo $message['file'];?>
              <?php else:?>
                Not found
              <?php endif;?>

            </strong>
          </small>
        </div>
      </div>
    <?php endforeach;?>
  </div>
</div>