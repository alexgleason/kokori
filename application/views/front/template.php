<!doctype>
<html>
  <head>
    <?php if ($data["meta_title"]):?>
      <title><?php echo $data["meta_title"];?></title>
    <?php endif;?>
    <?php if ($data["meta_description"]):?>
      <meta name="description" content="<?php echo $data["meta_description"];?>"/>
    <?php endif;?>
    <?php if ($data["meta_robots"]):?>
      <meta name="robots" content="<?php echo $data["meta_robots"];?>"/>
    <?php endif;?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="/themes/solarus/assets/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/themes/solarus/assets/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/themes/solarus/assets/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/themes/solarus/assets/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/themes/solarus/assets/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/themes/solarus/assets/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/themes/solarus/assets/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/themes/solarus/assets/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/themes/solarus/assets/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/themes/solarus/assets/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/themes/solarus/assets/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/themes/solarus/assets/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/themes/solarus/assets/icons/favicon-16x16.png">
    <link rel="manifest" href="/themes/solarus/assets/icons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffbc00">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffbc00">
    <?php echo HTML::style('assets/plugins/bootstrap/css/bootstrap.min.css');?>
    <?php echo HTML::style('assets/plugins/animate/animate.css');?>
    <?php echo HTML::style('assets/plugins/prism/prism.css');?>
    <?php echo HTML::style('assets/plugins/owl/dist/assets/owl.carousel.min.css');?>
    <?php echo HTML::style('assets/plugins/fancybox/dist/jquery.fancybox.min.css');?>
    <?php echo HTML::style('assets/css/front.css');?>
    <?php echo HTML::style('assets/css/front-mobile.css');?>

  </head>
  <body>
    <div class="container-full">
      <?php echo View::factory('front/header', array('data' => $data));?>
      <?php if (isset($data['entry']) && $data['entry']['entity']['has_breadcrumb']['value']):?>
        <?php echo View::factory('front/breadcrumb', array('data' => $data));?>
      <?php endif;?>
      <?php echo $content;?>
      <?php echo View::factory('front/footer', array('data' => $data));?>
    </div>
    <?php echo HTML::script('assets/plugins/jquery/jquery.min.js');?>
    <?php echo HTML::script('assets/plugins/popper/popper.min.js');?>
    <?php echo HTML::script('assets/plugins/bootstrap/js/bootstrap.min.js');?>
    <?php echo HTML::script('assets/plugins/fontawesome/js/all.js');?>
    <?php echo HTML::script('assets/plugins/appear/appear.js');?>
    <?php echo HTML::script('assets/plugins/owl/dist/owl.carousel.min.js');?>
    <?php echo HTML::script('assets/plugins/fancybox/dist/jquery.fancybox.min.js');?>
    <?php echo HTML::script('assets/plugins/paroller/dist/jquery.paroller.min.js');?>
    <?php echo HTML::script('assets/plugins/isotope/isotope.min.js');?>
    <?php echo HTML::script('assets/plugins/prism/prism.js');?>
    <?php echo HTML::script('assets/js/front.js');?>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
  </body>
</html>