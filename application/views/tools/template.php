<!doctype>
<html>
<head>
  <title>Tools</title>
  <?php echo HTML::style('assets/plugins/bootstrap/css/bootstrap.min.css');?>
  <?php echo HTML::style('assets/plugins/animate/animate.css');?>
  <?php echo HTML::style('assets/css/tools.css');?>

  <?php echo HTML::style('assets/css/front.css');?>

</head>
<body>
  <div class="container-fluid">
    <?php echo View::factory('front/header', array('data' => $data));?>
    <?php echo $content;?>
    <?php echo View::factory('front/footer', array('data' => $data));?>
  </div>
  <?php echo HTML::script('assets/plugins/jquery/jquery.min.js');?>
  <?php echo HTML::script('assets/plugins/bootstrap/js/bootstrap.min.js');?>
  <?php echo HTML::script('assets/plugins/fontawesome/js/all.js');?>
  <?php echo HTML::script('assets/plugins/appear/appear.js');?>
  <?php echo HTML::script('assets/js/tools.js');?>
</body>
</html>